#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"

// TODO: Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL

struct home* readhomes(char *filename){
    int ch=0;
    int lines=0;
    int x=0;
  
    //read entire file into memory
    FILE *fp = fopen(filename,"r");
    if(!fp){
        printf("cant open %s for read\n",filename);
        exit(1);
    } 
    
    while(!feof(fp))
    {
      ch = fgetc(fp);
      if(ch == '\n')
      {
        lines++;
      }
    }
    //printf("lines: %d\n",lines);
    //struct home homearray[lines];
   // lines++;
   home* homearray = malloc(lines * sizeof *homearray);
    for (x = 0; x < lines; x++){
        homearray[x].zip=(int)NULL;
        homearray[x].addr=NULL;
        homearray[x].price=(int)NULL;
        homearray[x].area=(int)NULL;
    }
    //fseek(fp, 0, SEEK_SET);
    rewind(fp);
    for (x = 0; x < lines; x++){
        homearray[x].zip=(int)malloc(sizeof(int));
        homearray[x].addr=(char*)malloc(sizeof(char*));
        homearray[x].price=(int)malloc(sizeof(int));
        homearray[x].area=(int)malloc(sizeof(int));
       fscanf(fp,"%d,%[^,],%d,%d",&homearray[x].zip,homearray[x].addr,&homearray[x].price,&homearray[x].area);
    }

    //for (x = 0; x < lines; x++)
      //printf("%d,%s,%d,%d",homearray[x].zip,homearray[x].addr,homearray[x].price,homearray[x].area);
    fclose(fp);  
    return homearray;
}

int homecount(struct home* array){
    int count=0;
    //int count = sizeof(array)/sizeof(array[0]);
    for(int i = 0; array[i].zip !=(int) NULL; i++){
        count++;
    }
    return count;
}

int zipinput(){
    int input;
    int valid=1;

    while (valid){
        printf("Enter zip code : ");
        scanf("%d",&input);
        if (input>=1 && input<=100000){
            valid=0;
        }
        else{
            printf("Zip code out of range! try Again.\n");
        }
}
    return input;
}

int countzip(struct home* array,int zipcode){
    int count=0;
    
    for(int i = 0; array[i].zip !=(int) NULL; i++){
        if(array[i].zip==zipcode){
            count++;
        }
    }
    
    return count;
}

int priceinput(){
    int input;
    int valid=1;

    while (valid){
        printf("Enter house price : ");
        scanf("%d",&input);
        if (input>=1 && input<=100000000){
            valid=0;
        }
        else{
            printf("Price out of range! try Again.\n");
        }
}
    return input;
}

void pricesearch(struct home* array,int userprice){
    int count=0;
    
    for(int i = 0; array[i].price !=(int) NULL; i++){
        if(abs(array[i].price-userprice)<=10000){
            printf("House price: %8d\t Address: %s \n",array[i].price, array[i].addr );
            count++;
        }
    }
    printf("Number of homes within this price range: %d\n",count);
}

void clear(struct home* array){
     for (int x = 0; array[x].price !=(int) NULL; x++){
        free(array[x].zip);
        free(array[x].addr);
        free(array[x].price);
        free(array[x].area);
    }
}

int main(int argc, char *argv[])
{
    
    // TODO: Use readhomes to get the array of structs
    home *homearray=readhomes("listings.csv");
    
    // At this point, you should have the data structure built
    // TODO: How many homes are there? Write a function, call it,
    // and print the answer.
    
    int count = homecount(homearray);
    printf("Number of houses : %d\n",count);
    
    // TODO: Prompt the user to type in a zip code.
    int userzip= zipinput();
    
    // At this point, the user's zip code should be in a variable
    // TODO: How many homes are in that zip code? Write a function,
    // call it, and print the answer.
    int ziphouses= countzip(homearray,userzip);
    printf("Number of houses with this zipcode : %d\n",ziphouses);
    
    // TODO: Prompt the user to type in a price.
    int userprice= priceinput();

    // At this point, the user's price should be in a variable
    // TODO: Write a function to print the addresses and prices of 
    // homes that are within $10000 of that price.
    pricesearch(homearray,userprice);
    
    clear(homearray);
    
    free(homearray);
    
    
    
}
